import request from './request'
import {stringify} from 'querystring';

const publish = (entity, update, mercureUrl, baseUrl, jwt) => {
    let data = stringify({
        topic: entity,
        data: JSON.stringify(update),
    });
    const requestData = {
        method: 'post',
        entity: data,
        path: mercureUrl,
        headers: {
            'Access-Control-Allow-Origin':  baseUrl,
            'Access-Control-Allow-Methods': 'POST, GET, HEAD',
            'Access-Control-Allow-Headers': 'Content-Length,Content-Type,Accept,Authorization',
            'Access-Control-Max-Age':       '0',
            'Authorization': 'Bearer ' + jwt,
            'Content-Type':    'application/x-www-form-urlencoded',
            'Accept': 'application/json',
        }
    };
    return request(requestData);
}

export { publish }
