import { publish } from './mercure.js';

jest.mock("./request.js");

// A simple example test
describe('#publish()', () => {
    test('should load data', (done) => {
        return publish('http://localhost/api/vnglst', {"a": 1})
            .then(data => {
                expect(data.entity).toBeDefined();
                done();
            });
    })
})
