// Adapted from https://github.com/apollographql/graphql-subscriptions/blob/master/src/test/tests.ts
const { isAsyncIterable } = require( "iterall" );
const { MercurePubSub } = require( "./mercure-pubsub.js" );
const { apiUrl, baseUrl, mercureUrl, jwt } = require( "./config.js" );

jest.setTimeout(30000);

const origLocation = document.location.href;

const flushPromises = () => {
  return new Promise(resolve => setImmediate(resolve));
};

let ps;

describe("MercurePubSub", () => {
    beforeEach(() => {
        ps = new MercurePubSub({
            baseUrl: baseUrl,
            apiUrl: apiUrl,
            mercureUrl: mercureUrl,
            jwt: jwt,
        });
    });

    afterEach(() => {
        ps.ee.end();
    });

    test("MercurePubSub can subscribe and is called when events happen", async done => {
        expect.assertions(2);
        await ps.subscribe("ghijkl", payload => {
            expect(payload.field).toEqual("test");
            done();
        }).then((id) => {
            const success = ps.publish(
                "ghijkl",
                {
                    "@id":  apiUrl + "ghijkl/1",
                    "id":   "1",
                    "field": "test"
                }
            );
            expect(id).toBe(1);
        }).catch(err => {
            console.log(err);
            done();
        });
    });

    test("MercurePubSub can unsubscribe", async done => {
        expect.assertions(2);
        const spy = jest.fn();
        await ps.subscribe("abcdef", spy).then((subId) => {
            ps.unsubscribe(subId);
            ps.publish("abcdef", {
                "@id":  apiUrl + "abcdef/1",
                "id":   "1",
                "field": "test"
            })
            .then((res) => {
                expect(res).toBe(false);
            }).finally(() => {
                done();
            });
            expect(subId).toBe(1);
        });
    });

    test("AsyncIterator should expose valid asyncIterator for a specific event", async done => {
        const eventName = "test";
        const iterator = ps.asyncIterator(eventName);
        expect(iterator).not.toBeUndefined();
        expect(isAsyncIterable(iterator)).toBe(true);
        done();
    });


    test("AsyncIterator should trigger event on asyncIterator when published", done => {
        expect.assertions(3);
        const eventName = "test5";
        const iterator = ps.asyncIterator(eventName);
        iterator.next().then(result => {
            expect(result).not.toBeUndefined();
            expect(result.value).not.toBeUndefined();
            expect(result.done).not.toBeUndefined();
            done();
        });
        return ps.publish(eventName, {
            "@id": apiUrl + eventName + '/5',
            id: 5,
            test: true,
            field: "test"
        });
    });

    test("AsyncIterator should not trigger event on asyncIterator when publishing other event", async done => {
        const eventName = "test2";
        const iterator = ps.asyncIterator("test");
        const spy = jest.fn();
        iterator.next().then(spy);
        await ps.publish(eventName, {
            "@id": apiUrl + eventName + '/2',
            id: 2,
            test: true
        }).then(
            (res) => console.log(res)
        ).catch(
            (err) => console.error(err)
        ).finally(() => {
            done();
        });
        expect(spy).not.toHaveBeenCalled();
    });

    test("AsyncIterator should register to multiple events", async done => {
        expect.assertions(1);
        const eventName = "test3";
        const iterator = ps.asyncIterator(["test3", "test4"]);
        const spy = jest.fn();
        iterator.next().then((v) => {
            spy();
            expect(spy).toHaveBeenCalled();
            done();
        });
        await ps.publish(eventName, {
            "@id": apiUrl + eventName + '/3',
            id: 3,
            test: true
        });
    });

  // test("AsyncIterator transforms messages using commonMessageHandler", done => {
  //   const eventName = "test";
  //   const commonMessageHandler = message => ({ transformed: message });
  //   const ps = new MercurePubSub({ topics, commonMessageHandler });
  //   const iterator = ps.asyncIterator(eventName);

  //   iterator.next().then(result => {
  //     expect(result).not.toBeUndefined();
  //     expect(result.value).toEqual({ transformed: { test: true } });
  //     expect(result.done).toBe(false);
  //     done();
  //   });

  //   ps.publish(eventName, { test: true });
  // });

  // test("MercurePubSub transforms messages using commonMessageHandler", function(done) {
  //   const commonMessageHandler = message => ({ transformed: message });
  //   const ps = new MercurePubSub({ topics, commonMessageHandler });
  //   ps.subscribe("transform", payload => {
  //     expect(payload).toEqual({ transformed: { test: true } });
  //     done();
  //   }).then(() => {
  //     const succeed = ps.publish("transform", { test: true });
  //     expect(succeed).toBe(true);
  //   });
  // });

  // This test does not clean up after it ends. It breaks the test that follows after it.
  // It won't break any tests if it's the last. https://imgflip.com/i/2lmlgm
  // TODO: Fix it properly
    test("AsyncIterator should not trigger event on asyncIterator already returned", async done => {
        expect.assertions(4);

        const eventName = "test6";
        const iterator = ps.asyncIterator(eventName);

        iterator.next().then(result => {
            expect(result).not.toBeUndefined();
            expect(result.value).not.toBeUndefined();
            expect(result.done).toBe(false);
        });

        await ps.publish(eventName, {
            "@id": apiUrl + eventName + '/6',
            id: 6,
            test: true
        });

        await iterator.return();
        const spy = jest.fn();
        iterator.next().then(result => {
            spy();
        });
        await expect(spy).not.toHaveBeenCalled();
        done();
    });
});
