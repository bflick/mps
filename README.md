Mercure Pubsub is outlined on the mercure page. There are examples of other
graphql-subscription compatible pubsubs, so I created one as the mercure
documentation suggests [mercure](https://github.com/dunglas/mercure#how-to-use-mercure-with-graphql). I came across mercure when looking into the openapi bundle for symfony php framework. There isn't much in the database used for testing currently, but a graphql node on your local machine can be streamed with mercure and javascript's eventsource interface.

Install:
```npm install```

Test:
```npm test```

The tests show how to use the component (and how not to). I will add examples as I begin using it more.