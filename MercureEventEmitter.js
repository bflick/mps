import { EventEmitter } from 'events';
import EventSource from 'eventsource';
import { publish } from './api/mercure.js';
import { parse } from 'querystring';

var RESERVED_CHANNELS = ['newListener', 'removeListener', 'notified', 'notify', 'unlisten', 'listen', 'error', 'end'];

export class MercureEventEmitter extends EventEmitter {
    constructor(url, apiUrl, baseUrl, jwt) {
        super();
        let self = this;
        this.jwt         = jwt;
        this.baseUrl     = baseUrl;
        this.apiUrl      = apiUrl;
        this.urlString   = url;
        this.url         = new URL(url);
        this.pubFeedback = {};
        this.currentTopics = [];
        this.topics        = [];

        this.eventSourceInitDict = {
            headers: {
                'Authorization': 'Bearer ' + jwt,
            }
        };

        this.on('newListener', function (channel, fn) {
            if (RESERVED_CHANNELS.indexOf(channel) < 0
                && this.listenerCount(channel) === 0) {
                self.addTopic(channel, fn);
            }
        });

        this.on('removeListener', function (channel, fn) {
            if (RESERVED_CHANNELS.indexOf(channel) < 0
                && this.listenerCount(channel) === 0) {
               self.removeTopic(channel);
            }
        });

        this.on('listen', this.newEventSource);
        this.on('unlisten', this.newEventSource);

        this.onerror = function (err) {
            console.error(err);
        }
    }

    newEventSource() {
        if (!this.eventSource || this.currentTopics != this.topics) {
            if (this.eventSource
                && this.eventSource.readyState === EventSource.CONNECTING) {
                setTimeout(this.newEventSource.bind(this), 250);
                return;
            }
            if (this.eventSource
                && this.eventSource.readyState !== EventSource.CLOSED) {
                this.eventSource.close();
            }
            const topics = this.topics;
            const self   = this;
            if (this.topics.length > 0) {
                this.eventSource = new EventSource(this.url.toString(), this.eventSourceInitDict);
                this.eventSource.onmessage = function (message) {
                    try {
                        const messageData = JSON.parse(message.data);
                        if (message.data && messageData['@id']) {
                            const urlStrings = messageData['@id'].split('/');
                            const topic = urlStrings[urlStrings.length - 2];
                            self.emit(topic, messageData);
                            self.emit('notified', topic);
                            self.pubFeedback[message['lastEventId']] = true;
                        } else {
                            self.emit('error', {"Error": "malformed eventSource response", "Message": message});
                        }
                    } catch (ex) {
                        // Weird that jest highlighted this line when expect failed
                        self.emit('error', ex);
                    }
                };
                this.eventSource.onopen = function (res) {
                    self.currentTopics = topics;
                };
                this.eventSource.onerror = function (err) {
                    console.error(err);
                    self.currentTopics = [];
                    self.emit('error', err);
                };
            } else {
                this.eventSource = null;
            }
        }
    }

    send (channel, payload) {
        if (!payload.id) {
            return Promise.reject(payload);
        }
        const self = this;
        return new Promise((resolve, reject) => {
            let pubRef, req, retry;
            if (this.eventSource
                && this.eventSource.readyState === EventSource.CONNECTING) {
                reject(false);
            } else {
                const path = this.apiUrl + channel + '/' + payload.id + '.jsonld';
                req = publish(
                    path,
                    payload,
                    this.urlString,
                    this.baseUrl,
                    this.jwt,
                )
                .then((res) => {
                    return res;
                })
                .catch((err) => {
                    this.emit('error', err);
                    reject(err);
                })
                .then((res) => {
                    pubRef = res.entity;
                    resolve(pubRef);
                    this.pubFeedback[pubRef] = false;
                });
            }
        });
    }

    notify(channel, payload) {
        let p = Promise.reject();
        // @todo config
        var t = 2000;
        let max = 10;

        function rejectDelay(reason) {
            return new Promise(function(resolve, reject) {
                setTimeout(reject.bind(null, reason), t);
            });
        }

        for (let i = 0; i < max; i++) {
            p = p.catch(this.send.bind(this, channel, payload)).then((res) => {
                if (!res.entity) {
                    throw false;
                } else {
                    return res;
                }
            }).catch(rejectDelay);
        }

        return p.then((res) => res)
            .catch((result) => {
                return false;
            })
    }

    end() {
        if (this.eventSource
            && this.eventSource.readyState !== EventSource.CLOSED) {
            this.eventSource.close();
        }
        this.removeAllListeners();
        this.emit('end');
    }

    addTopic(topic) {
        if (topic && this.topics.indexOf(topic) === -1) {
            this.topics.push(topic);
            this.url.searchParams.append('topic', this.apiUrl + topic + '/{id}.jsonld');
        }
        this.emit('listen', topic);
    }

    removeTopic(topic) {
        if (topic && this.topics.indexOf(topic) !== -1) {
            let newTopics = [];
            this.url = new URL(this.urlString);
            for (let i in this.topics) {
                if (this.topics[i] !== topic) {
                    newTopics.push(this.topics[i]);
                }
            }
            this.topics = newTopics;
            for (let i in this.topics) {
                let t = this.topics[i];
                this.url.searchParams.append('topic', this.apiUrl + t + '/{id}.jsonld');
            }
        }
        this.emit('unlisten', topic);
    }
}
