import { PubSub } from "graphql-subscriptions";
import { eventEmitterAsyncIterator } from "./event-emitter-to-async-iterator";
import { MercureEventEmitter } from "./MercureEventEmitter.js";

const defaultCommonMessageHandler = message => {
    return message
};

export class MercurePubSub extends PubSub {
    constructor(options = {}) {
        const { commonMessageHandler, apiUrl, mercureUrl, baseUrl, jwt } = options;
        super();
        this.commonMessageHandler = commonMessageHandler || defaultCommonMessageHandler;
        this.ee                   = new MercureEventEmitter(mercureUrl, apiUrl, baseUrl, jwt);
        this.subscriptions        = {};
        this.subsRefsMap          = {};
        this.subIdCounter         = 0;
    }

    publish(triggerName, payload) {
            return this.ee.notify(triggerName, payload).then((ref) => {
		return ref;
            })
    }

    subscribe(triggerName, onMessage) {
        const callback = message => {
            onMessage(
                message instanceof Error
                    ? message
                    : this.commonMessageHandler(message)
            );
        };
        this.ee.on(triggerName, callback);
        this.subIdCounter += 1;
        this.subscriptions[this.subIdCounter] = [triggerName, callback];
        if (!Array.isArray(this.subsRefsMap[triggerName])
            || this.subsRefsMap[triggerName].length === 0) {
            this.subsRefsMap[triggerName] = [this.subIdCounter];
        } else {
            this.subsRefsMap[triggerName].push(this.subIdCounter);
        }
        return Promise.resolve(this.subIdCounter);
    }

    unsubscribe(subId) {
        const [triggerName, onMessage] = this.subscriptions[subId];
        delete this.subscriptions[subId];
        this.subsRefsMap[triggerName].splice(this.subsRefsMap[triggerName].indexOf(subId));
        this.ee.removeListener(triggerName, onMessage);
    }

    asyncIterator(triggers) {
        return eventEmitterAsyncIterator(
            this.ee,
            triggers,
            this.commonMessageHandler
        );
    }
}
